<?php
require_once('dbconn.php');
$id=$_GET['id'];
$sql="SELECT* FROM users where id=$id";
$result=mysqli_query($conn, $sql);
if(!$result){
  header("Location: list.php?msg=not_found");
}
$value = mysqli_fetch_assoc($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <title>List User Details</title>
</head>
<body>
<div class="container">
    <h1 class="text-center">User Details:</h1>
    <a href="list.php" class="btn btn-primary">Back</a>
    <div class="card">
    <center>
    <img src="uploads/<?php echo $value['photo'] ?>" class="img-fluid rounded-circle mt-2" width="200px" height="200px">
    </center>
    <div class="card-body text-center">
      <h5 class="card-title"><?php echo $value['name'] ?></h5>
      <p class="card-text"><?php echo $value['email'] ?></p>
      <p class="card-text badge badge-info"><?php echo ucfirst($value['status']) ?></p>
      <p class="card-text"><?php echo ($value['is_admin']==1)?"Admin":"Normal User" ?></p>
      <p class="card-text"><small class="text-muted"><?php echo $value['created_at'] ?></small></p>
    </div>
  </div>
</div>
</body>
</html>