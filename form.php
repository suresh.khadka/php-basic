<?php
require_once('check_session.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <title>Form</title>
</head>
<body>
  <div class="container">
  <h4 class="text-center">User Data</h4>
  <p class="float-right"><a href="list.php" class="btn btn-primary">List Users</a></p><br>
  <hr>
  <form action="form_data.php" method="POST" enctype="multipart/form-data">
    <label for="">Name:</label>
    <input type="text" name="name" id="" class="form-control" required>

    <label for="">Email:</label>
    <input type="email" name="email" id="" class="form-control" required>

    <label for="">Password:</label>
    <input type="password" name="password" id="" class="form-control" required>
    <br>
    <label for="">Is Admin:</label>
    <input type="radio" name="is_admin" value="1" id=""> Yes
    <input type="radio" name="is_admin" value="0" id="" checked> NO
    <br>
    <label for="">Status:</label>
      <select name="status" id="" class="form-control" required>
      <option value="">Status</option>
      <option value="active">Active</option>
      <option value="inactive">Inactive</option>
      </select>
  <br>
    <input type="file" name="photo" id="" required accept="image/*">
    <br>
    <br>
    <input type="submit" value="Sumbit" class="btn btn-primary">
  </form>
  </div>
</body>
</html>