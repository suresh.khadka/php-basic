<?php
require_once('check_session.php');
require_once('dbconn.php');
$sql="SELECT* FROM users ORDER BY id DESC";
$result=mysqli_query($conn,$sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
  <title>List Student</title>
</head>
<body>
<div class="container">
<div class="row">
  <div class="col-md-6">
  User:<?php echo $_SESSION['name']; ?><br>
  Email:<?php echo $_SESSION['email']; ?><br>
  <img src="uploads/<?php echo $_SESSION['photo']  ?>" alt="" width="100" height="100" class="img-fluid">
  </div>
  <div class="col-md-6">

  </div>
</div>
<h3 class="text-center">List Users</h3>
<p class="mt-4"><a href="form.php" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add New Student</a></p>
<p class="mt-4"><a href="logout.php" class="btn btn-primary btn-sm"><i class="fa fa-logout"></i> Logout</a></p>
<?php
  @$msg=$_GET['msg'];
  if (@$msg == 'succcess') {?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Success!</strong> User Created Sucessfully.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
  <?php
  }else if(@$msg=='updated'){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Success!</strong> User Updated Sucessfully.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <?php
  }else if(@$msg=='del_success'){ ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Success!</strong> User Deleted Sucessfully.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
  <?php } else if(@$msg=='update_error'){ ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>Error!</strong> Error While Updating user.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <?php } else if(@$msg=='del_error'){ ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>Error!</strong> Error While Deleting user.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <?php } else if(@$msg=='error'){ ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>Error!</strong> Error While Creating user.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <?php } ?>
  <table class="table table-striped">
  <thead>
    <th>Sn</th>
    <th>Name</th>
    <th>Email</th>
    <th style="width:10%">Photo</th>
    <th>Is Admin</th>
    <th>Status</th>
    <th>Created At</th>
    <th width="10%">Action</th>
  </thead>
  <tbody>
<?php
if (mysqli_num_rows($result) > 0) {
    $i = 1;
    while ($value = mysqli_fetch_assoc($result)) {
        ?>
  <tr>
    <td><?php echo $i; ?>.</td>
    <td><?php echo $value['name'] ?></td>
    <td><?php echo $value['email'] ?></td>
    <td>
    <?php if ($value['photo'] != null && file_exists('uploads/' . $value['photo'])) { ?>
      <img src="uploads/<?php echo $value['photo'] ?>" class="img-fluid img-thumbnail"  alt="">
      <?php } ?>
    </td>
    <td><?php echo ($value['is_admin'] == 1) ? 'Admin' : 'Normal User' ?></td>
    <td><?php echo ($value['status'] == 'active') ? 'Active' : 'Inactive' ?></td>
    <td><?php echo $value['created_at'] ?></td>
    <td>
      <div class="btn-group">
      <a href="show.php?id=<?php echo $value['id'] ?>" class="btn btn-sm btn-primary"> <i class="fas fa-eye"></i></a>
      <a href="edit_form.php?id=<?php echo $value['id'] ?>" class="btn btn-sm btn-info"><i class="fas fa-edit"></i></a>
      <a href="delete.php?id=<?php echo $value['id']?>&img=<?php echo $value['photo'] ?>" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
      </div>
    </td>
  </tr>
  <?php $i++;
    }
}else{
?>
  <tr>
    <td colspan="7">User not found in database</td>
  </tr>
<?php } ?>
  </tbody>
</div>
</table>
</body>
</html>