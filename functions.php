<?php

function upload_file($file){
  $upload_dir='uploads/';
  $target_file = $upload_dir . basename($file["photo"]["name"]);
  if (move_uploaded_file($file["photo"]["tmp_name"], $target_file)) {
     return $file['photo']['name'];
  } else {
      return false;
  }
}