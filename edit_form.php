<?php
require_once('check_session.php');
require_once('dbconn.php');
$id=$_GET['id'];
if($id==''){
  header("Location: list.php");
}
$sql="SELECT * FROM users WHERE id=$id";
$result=mysqli_query($conn, $sql);
if(!mysqli_num_rows($result) > 0){
  header("Location: list.php");
}
$value = mysqli_fetch_assoc($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <title>Form</title>
</head>
<body>
  <div class="container">
  <h4 class="text-center">Edit User</h4>
  <p class="float-right"><a href="list.php" class="btn btn-primary">Back</a></p><br>
  <hr>
  <form action="edit_data.php" method="POST" enctype="multipart/form-data">
    <label for="">Name:</label>
    <input type="text" value="<?php echo $value['name'] ?>" name="name" id="" class="form-control">
    <input type="hidden" name="id" value="<?php echo $value['id'] ?>" id="">
    <label for="">Email:</label>
    <input type="email" value="<?php echo $value['email'] ?>" name="email" id="" class="form-control">

    <label for="">Password:</label>
    <input type="password" name="password" id="" class="form-control">
    <br>
    <label for="">Is Admin:</label>
    <?php if($value['is_admin']==1){ ?>
    <input type="radio" name="is_admin" value="1" id="" checked> Yes
    <input type="radio" name="is_admin" value="0" id=""> NO
    <?php }else{
      ?>
     <input type="radio" name="is_admin" value="1" id=""> Yes
      <input type="radio" name="is_admin" value="0" id="" checked> NO
    <?php
    } ?>

    <br>
    <label for="">Status:</label>
      <select name="status" id="" class="form-control">
      <option value="">Status</option>
      <?php if($value['status']=='active'){ ?>
      <option value="active" selected>Active</option>
      <option value="inactive">Inactive</option>
      <?php }else{
      ?>
       <option value="active">Active</option>
      <option value="inactive" selected>Inactive</option>
    <?php
    } ?>
      </select>
  <br>
    <input type="file" name="photo" id="" accept="image/*">
    <input type="hidden" name="old_photo" value="<?php echo $value['photo'] ?>" id="">
    <?php if($value['photo']!='' && file_exists('uploads/'.$value['photo'])){ ?>
      <img src="uploads/<?php echo $value['photo'] ?>" alt="" class="img-fluid img-thumbnail" width="40%">
    <?php } ?>
    <br>
    <br>
    <input type="submit" value="Sumbit" class="btn btn-primary">
  </form>
  </div>
</body>
</html>